$( document ).ready(function() {
    let redf_grid   = [1, 1, 2, 1]; //Red frame is placed at 1x1 and is a 2x1 rectangle
    let greenf_grid = [3, 1, 1, 2]; //Green frame is at 3x1 and is a 1x2 rectangle
    let bluef_grid  = [1, 2, 1, 1]; //Blue frame is at 2x1 and is a 1x1 rectangle
    let yelf_grid   = [1, 3, 2, 2]; //Yellow frame is at 1x3 and is a 2x2 rectangle
    let magf_grid   = [3, 4, 1, 1]; //Magenta frame is at 3x4 and is a 1x1 rectangle
    let cyanf_grid  = [2, 2, 1, 1]; //Cyan is at 2x2 and is a 1x1 rectangle
    let whitf_grid  = [3, 3, 1, 1]; //White is at 3x3 and is a 1x1 rectangle

    let gridbag = [[1,1,1,1], [1, 1], "both", "center", [0, 0], [50, 50]];

    gridbag[0] = redf_grid;
    addItem("red", ...gridbag);

    gridbag[0] = greenf_grid;
    gridbag[5] = [500, 50];
    gridbag[1] = [0.1, 1];
    addItem("green", ...gridbag);

    gridbag[0] = bluef_grid;
    gridbag[1] = [1, 1];
    gridbag[2] = "none";
    gridbag[5] = [50, 50];
    addItem("blue", ...gridbag);

    gridbag[0] = cyanf_grid;
    gridbag[2] = "vertical";
    gridbag[3] = "right";
    gridbag[4] = [100, 0];
    addItem("cyan", ...gridbag);

    gridbag[0] = yelf_grid;
    gridbag[2] = "both";
    gridbag[3] = "center";
    gridbag[4] = [0, 0];
    addItem("yellow", ...gridbag);

    gridbag[0] = whitf_grid;
    gridbag[1] = [0.1, 1];
    addItem("white", ...gridbag);

    gridbag[0] = magf_grid;
    gridbag[1] = [0.1, 1];
    addItem("magenta", ...gridbag);

    updateGridBag();
});

function addItem(color, grid, weight, fill, anchor, padding, preferredsize) {
    //console.log("addItem", grid, weight, fill, anchor, padding, preferredsize);
    let item = $("<div>").addClass("GO_GRIDBAG");
    item.data("gridbag", {grid,weight, fill, anchor, padding, preferredsize});
    item.css({
        "grid-column-start": grid[0],
        "grid-column-end": grid[0] + grid[2],
        "grid-row-start": grid[1],
        "grid-row-end": grid[1] + grid[3],
        "padding": `${padding[1]}px ${padding[0]}px`
    });

    let sub = $("<div>").css({"background": color});

    switch(fill) {
        case "both":
            sub.css({"width": "100%", "height": "100%"});
            break;
        case "vertical":
            sub.css({"width": `${preferredsize[0]}px`, "height": "100%"});
            break;
        case "horizontal":
            sub.css({"width": "100%", "height": `${preferredsize[1]}px`});
            break;
        case "none":
            sub.css({"width": `${preferredsize[0]}px`, "height": `${preferredsize[1]}px`});
            break;
    }

    switch(anchor) {
        case "center":
            item.css({"place-items": "center"});
            break;
        case "right":
            item.css({"place-items": "center end"});
            break;
        case "left":
            item.css({"place-items": "center start"});
            break;
        case "upper":
            item.css({"place-items": "start center"});
            break;
        case "lower":
            item.css({"place-items": "end center"});
            break;
        case "upper_right":
            item.css({"place-items": "start end"});
            break;
        case "lower_right":
            item.css({"place-items": "end end"});
            break;
        case "upper_left":
            item.css({"place-items": "start start"});
            break;
        case "lower_left":
            item.css({"place-items": "end start"});
            break;
        }

    item.append(sub);

    $(".GO_FRAME").append(item);
}

function updateGridBag() {
    //get max row
    let rows = $(".GO_GRIDBAG").map(function() { return $(this).css("grid-row-end") - 1;});
    rows = Math.max(...rows);

    let rowSum = [];
    for (let i = 0; i < rows; ++i) {
        rowSum[i] = 0;
        let items = $(".GO_GRIDBAG").filter(function() {
            let data = $(this).data("gridbag");
            //console.log(i+1, ">=", data.grid[1], "&&", i+1, "<", data.grid[1] + data.grid[3]);
            return i+1 >= data.grid[1] && i+1 < data.grid[1] + data.grid[3];
        });

        //console.log(`items[${i}]`, items);

        items.each(function() {
            rowSum[i] += $(this).data("gridbag").weight[0] * $(this).data("gridbag").grid[2];
        })
    }

    $(".GO_GRIDBAG").each(function() {
        let data = $(this).data("gridbag");
        console.log("data", data.grid);
        let sum = rowSum[data.grid[1]-1];
        let r = data.weight[0] / sum;
        let rs = Math.floor((data.grid[0] - 1) * 100 / sum) + 1;
        let re = Math.floor((data.grid[2] * 100) * r);
        console.log("row", $(this).css("grid-area"), sum, r, rs, rs+re);
        $(this).css({
            "grid-column-start": rs,
            "grid-column-end": rs + re
        })
    })
    console.log("rowSum", rowSum);
}
